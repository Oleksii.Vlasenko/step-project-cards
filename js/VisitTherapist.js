class VisitTherapist extends Visit{

    static MEDICAL_SPECIALIZATION = `Терапевт`;

    static inputLines = {
        title: [`Мета візиту`, `text`, `Вкажіть мету візиту пацієнта`],
        visitDate: [`Дата візиту`, `date`],
        patient: [`ПІП`, `text`, `Повне ім'я пацієнта`],
        age: [`Вік`, `number`, `Вік пацієнта`]
    };

    constructor({title, visitDate, patient, age, comment, id}) {
        super(title, visitDate, patient, comment, id);
        this._age = age;
        this._doctor = VisitTherapist.MEDICAL_SPECIALIZATION;
    }

    getDetails() {
        return {
            patient: this._patient,
            title: this._title,
            visitDate: this._visitDate,
            age: this._age
        }
    }

    edit({patient, title, visitDate, age, comment}) {
        super.edit({title, visitDate, patient, comment});
        this._age = age;
    }

    get comment() {
        return this._comment;
    }

    get patient() {
        return this._patient;
    }

    get doctor() {
        return this._doctor;
    }

    get id() {
        return this._id;
    }

    set id(id) {
        this._id = id;
    }

    get inputLines(){
        return VisitTherapist.inputLines;
    }
}