class VisitDOM {

    static editCard(event) {
        const card = event.target.parentElement;
        const cardId = card.getAttribute(`id`);
        const visit = Base.getVisitById(cardId);

        const detailsBlock = document.getElementsByClassName(`details`)[0];
        detailsBlock.classList.add(`active`);

        detailsBlock.addEventListener(`click`, function (event) {
            if (event.target === detailsBlock) {
                VisitDOM.hide(detailsBlock);
            }
        });

        const details = document.getElementById(`details-content`);
        details.innerHTML = "";

        const doctor = document.createElement(`h4`);
        doctor.innerHTML = `${visit.doctor}`;
        details.appendChild(doctor);

        for (let item in visit.getDetails()) {
            const line = document.createElement(`div`);
            line.classList.add(`details-more`);
            line.innerHTML = `<label>${visit.inputLines[item][0]}<br /><input type="${visit.inputLines[item][1]}" id="${item}" class="modal-input"></label>`;
            line.getElementsByTagName(`input`)[0].value = visit.getDetails()[item];
            details.appendChild(line);
        }

        const comment = document.createElement(`div`);
        comment.classList.add(`details-more`);
        comment.innerHTML = `<label>Додаткова інформація<br /><textarea maxlength="400" class="modal-comment">${visit.comment}</textarea></label>`;
        details.appendChild(comment);

        const editBtn = document.createElement(`button`);
        editBtn.classList.add(`card-show-more`);
        editBtn.innerHTML = `Edit visit`;

        editBtn.addEventListener(`click`, function (event) {
            const editedVisit = {};
            const inputs = document.getElementsByClassName(`modal-input`);
            const comment = document.getElementsByClassName(`modal-comment`)[0];

            for (let item of inputs) {
                editedVisit[`${item.getAttribute('id')}`] = item.value;
            }
            editedVisit[`comment`] = comment.value;
            console.log(comment.value);
            // const obj = Object.assign(visit);
            // obj.edit(editedVisit);
            visit.edit(editedVisit);
            Request.sendPutRequest(visit)
                .then(response => {
                    if (response.ok) {
                        Local.saveToLocalStorage(visit);
                        let h4 = card.getElementsByTagName(`h4`)[0];
                        h4.innerHTML = `${visit.patient}<br />${visit.doctor}`;
                    }
                });
            detailsBlock.classList.remove(`active`);
        });

        details.appendChild(editBtn);

        detailsBlock.appendChild(details);

    }

    static removeCard(event) {
        const card = event.target.parentElement;
        const board = document.getElementById(`board`);
        let visitId = card.getAttribute(`id`);
        let visit = Base.getVisitById(visitId);
        Request.sendDeleteRequest(visit)
            .then(status => {
                if (status) {
                    Local.removeFromLocalStorage(visit);
                    board.removeChild(card);
                    visit.remove();
                }
            });
        if (document.getElementsByClassName(`card`).length === 0) {
            const noVisit = document.getElementsByClassName(`board-text`)[0];
            noVisit.classList.add(`active`);
        }
    }

    static async createNewVisit(event, doctor) {
        let inputObject = {};
        const inputs = document.getElementsByClassName(`modal-input`);
        const comment = document.getElementsByClassName(`modal-comment`)[0];

        for (let item of inputs) {
            inputObject[`${item.getAttribute('id')}`] = item.value;
        }
        inputObject[`comment`] = comment.value;

        let visit;
        switch (doctor) {
            case 1:
                visit = new VisitTherapist(inputObject);
                break;
            case 2:
                visit = new VisitDentist(inputObject);
                break;
            case 3:
                visit = new VisitCardiologist(inputObject);
                break;
            default:
                return false;
        }
        // localStorage.setItem(`visit_${doctor}_${visit.id}`, JSON.stringify(visit));
        await Request.sendPostRequest(visit);
        Base.addVisit(visit);
        VisitDOM.createCard(visit);
    }

    static createCard(visit) {
        Local.saveToLocalStorage(visit);
        const noVisit = document.getElementsByClassName(`board-text`)[0];
        noVisit.classList.remove(`active`);
        const fragment = document.createDocumentFragment();

        const card = document.createElement(`div`);
        card.classList.add(`card`);
        card.setAttribute(`id`, `${visit.id}`);
        card.setAttribute(`draggable`, `true`);
        fragment.appendChild(card);

        card.ondragend = function (event) {
            VisitDOM.dragEnd(event);
        };

        const deleteBtn = document.createElement(`button`);
        deleteBtn.classList.add(`card-delete`);
        deleteBtn.innerHTML = `x`;
        deleteBtn.addEventListener(`click`, function (event) {
            VisitDOM.removeCard(event)
        });
        card.appendChild(deleteBtn);

        const cardHeader = document.createElement(`h4`);
        cardHeader.classList.add(`card-header`);
        cardHeader.innerHTML = `${visit.patient}<br />${visit.doctor}`;

        card.appendChild(cardHeader);

        const cardBtn = document.createElement(`button`);
        cardBtn.classList.add(`card-show-more`);
        cardBtn.addEventListener(`click`, function (event) {
            VisitDOM.showDetails(event)
        });
        cardBtn.innerText = `Show more`;
        card.appendChild(cardBtn);

        const editBtn = document.createElement(`button`);
        editBtn.classList.add(`card-show-more`);
        editBtn.innerHTML = `Edit visit`;
        editBtn.addEventListener(`click`, function (event) {
            VisitDOM.editCard(event);
        });
        card.appendChild(editBtn);

        const board = document.getElementById(`board`);
        board.appendChild(fragment);
    }

    static showDetails(event) {
        const visit = Base.getVisitById(event.target.parentElement.getAttribute(`id`));

        const detailsBlock = document.getElementsByClassName(`details`)[0];
        detailsBlock.classList.add(`active`);

        detailsBlock.addEventListener(`click`, function (event) {
            if (event.target === detailsBlock) {
                VisitDOM.hide(detailsBlock);
            }
        });

        const details = document.getElementById(`details-content`);
        details.innerHTML = "";

        const doctor = document.createElement(`h4`);
        doctor.innerHTML = `${visit.doctor}`;
        details.appendChild(doctor);

        for (let item in visit.getDetails()) {
            const line = document.createElement(`p`);
            line.classList.add(`details-more`);
            line.innerHTML = `${visit.inputLines[item][0]} : ${visit.getDetails()[item]}`;
            details.appendChild(line);
        }

        const comment = document.createElement(`p`);
        comment.innerHTML = `Додаткова інформація: ${visit.comment}`;
        details.appendChild(comment);

        detailsBlock.appendChild(details);

    }

    static createForm(lines, doctor) {
        const modal = document.getElementsByTagName(`form`)[0];
        modal.innerHTML = ``;
        const fragment = document.createDocumentFragment();
        for (let item in lines) {
            const element = document.createElement(`div`);
            element.innerHTML = `<label>${lines[item][0]}<br /><input type="${lines[item][1]}" id="${item}" class="modal-input"></label>`;
            if (lines[item][2]) {
                element.getElementsByTagName(`input`)[0].setAttribute(`placeholder`, `${lines[item][2]}`);
                element.getElementsByTagName(`input`)[0].onfocus = function () {
                    element.getElementsByTagName(`input`)[0].style.border = `1px solid #a8a8a8`;
                }
            }
            fragment.appendChild(element);
        }
        const textarea = document.createElement(`div`);
        textarea.innerHTML = `<label>Примітки<br /><textarea maxlength="400" class="modal-comment" placeholder="Додаткова інформація"></textarea></label>`;
        fragment.appendChild(textarea);

        const btnSubmit = document.createElement(`button`);
        btnSubmit.classList.add(`form-btn`);
        btnSubmit.setAttribute(`type`, `submit`);
        btnSubmit.setAttribute(`onclick`, `VisitDOM.submit(event, ${doctor})`);
        btnSubmit.innerHTML = `Створити візит`;
        fragment.appendChild(btnSubmit);

        modal.appendChild(fragment);
    }

    static clearForm() {
        const modal = document.getElementsByTagName(`form`)[0];
        modal.innerHTML = ``;
    }

    static show() {
        const modal = document.getElementsByClassName(`modal`)[0];
        modal.classList.add(`active`);
        modal.addEventListener(`click`, function (event) {
            if (event.target === modal) {
                VisitDOM.hide(modal);
            }
        })
    }

    static hide(element) {
        element.classList.remove(`active`);
    }

    static hideModal() {
        const modal = document.getElementsByClassName(`modal`)[0];
        VisitDOM.hide(modal);
    }

    static submit(event, doctor) {
        event.preventDefault();
        const modal = document.getElementsByClassName(`modal`)[0];
        let inputs = document.getElementsByClassName(`modal-input`);
        let flag = true;
        for (let i = 0; i < inputs.length; i++) {
            if (!inputs[i].value) {
                inputs[i].style.border = `1px solid #ff0000`;
                flag = false;
            }
        }
        if (flag) {
            VisitDOM.createNewVisit(event, doctor);
            VisitDOM.hide(modal);
        }
    }

    static dragEnd(event) {
        //hide event.target
        let col = event.target.parentElement;
        event.target.classList.add(`hidden`);

        //get element under event.target
        let element = document.elementFromPoint(event.clientX, event.clientY);
        //check: element is card? element.parent is null?
        if (!element.classList.contains(`card`) && element.parentElement) {
            //if element === card.child => element = element.parent
            if (element.parentElement.classList.contains(`card`)) {
                element = element.parentElement;
            }
        }
        //show event.target
        event.target.classList.remove(`hidden`);

        //check one more time ???
        if (!element.classList.contains(`card`)) return false;

        //get object with element and event.target coordinates
        let elementCoordinates = element.getBoundingClientRect();
        let parentCoordinates = event.target.parentNode.getBoundingClientRect();

        //check correct coordinates
        if (VisitDOM.isCorrectCoordinates(parentCoordinates, event)) {
            //if first part - before, if second part - after
            if ((elementCoordinates.x + elementCoordinates.width / 2) > event.clientX) {
                col.insertBefore(event.target, element);
            } else {
                col.insertBefore(event.target, element.nextSibling);
            }
        }
    }

    static isCorrectCoordinates(parent, child) {
        //check
        return parent.left <= child.clientX && parent.right >= child.clientX && parent.top <= child.clientY && parent.bottom >= child.clientY;
    }
}