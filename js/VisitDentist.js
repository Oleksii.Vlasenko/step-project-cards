class VisitDentist extends Visit {

    static MEDICAL_SPECIALIZATION = `Дантист`;

    static inputLines = {
        title: [`Мета візиту`, `text`, `Вкажіть мету візиту пацієнта`],
        lastVisit: [`Дата останнього візиту`, `date`],
        visitDate: [`Дата візиту`, `date`],
        patient: [`ПІП`, `text`, `Повне ім'я пацієнта`]
    };

    constructor({title, lastVisit, visitDate, patient, comment, id}) {
        super(title, visitDate, patient, comment, id);
        this._lastVisit = lastVisit;
        this._doctor = VisitDentist.MEDICAL_SPECIALIZATION;
    }

    getDetails() {
        return {
            patient: this._patient,
            title: this._title,
            lastVisit: this._lastVisit,
            visitDate: this._visitDate
        }
    }

    edit({patient, title, lastVisit, visitDate, comment}) {
        super.edit({title, visitDate, patient, comment});
        this._lastVisit = lastVisit;
    }

    get comment() {
        return this._comment;
    }

    get patient() {
        return this._patient;
    }

    get doctor() {
        return this._doctor;
    }

    get id() {
        return this._id;
    }

    set id(id) {
        this._id = id;
    }

    get inputLines(){
        return VisitDentist.inputLines;
    }
}