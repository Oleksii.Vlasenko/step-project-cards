class User {
    static getToken() {
        return sessionStorage.getItem(`token`);
    }
    static setToken(token) {
        sessionStorage.setItem(`token`, `${token}`);
    }
    static getUser() {
        return sessionStorage.getItem(`user`);
    }
    static setUser(user) {
        sessionStorage.setItem(`user`, user);
    }
    static getPassword() {
        return sessionStorage.getItem(`password`);
    }
    static setPassword(password) {
        sessionStorage.setItem(`password`, password);
    }
}

class Request {

    // static takeToken(user, password) {
    //     return fetch(`http://cards.danit.com.ua/login`, {
    //         method: `POST`,
    //         body: JSON.stringify({
    //             email: `${user}`,
    //             password: `${password}`
    //         })
    //     })
    // }

    static async authorization(user, password) {
        return  fetch(`http://cards.danit.com.ua/login`, {
            method: `POST`,
            body: JSON.stringify({
                email: `${user}`,
                password: `${password}`
            })
        })
            .catch(err => console.log(err));
    }

    static sendDeleteRequest(visit) {
        return fetch(`http://cards.danit.com.ua/cards/${visit.id}`,{
            method: `DELETE`,
            headers: {
                Authorization: `Bearer ${User.getToken()}`
            }
        })
            .then(response => {
                return response.ok;
            })
            .catch(err => console.log(err));
    }

    static sendPutRequest(visit) {
        console.log(visit);
        return fetch(`http://cards.danit.com.ua/cards/${visit.id}`, {
            method: `PUT`,
            headers: {
                Authorization: `Bearer ${User.getToken()}`
            },
            body: JSON.stringify(visit)
        })
    }

    static sendGetRequest() {
        return fetch(`http://cards.danit.com.ua/cards`, {
            method: `GET`,
            headers: {
                Authorization: `Bearer ${User.getToken()}`
            }
        });
    }

    static async sendPostRequest(visit) {
        await fetch(`http://cards.danit.com.ua/cards`, {
            method: `POST`,
            headers: {
                Authorization: `Bearer ${User.getToken()}`
            },
            body: JSON.stringify(visit)
        })
            .then(response => response.json())
            .then(obj => {
                visit.id = obj.id
            })
            .catch(err => console.log(err));
    }
}

class Local {

    static saveToLocalStorage(visit) {
        let doctor;
        switch(visit.doctor) {
            case `Терапевт`: doctor = 1;
            break;
            case `Дантист`: doctor = 2;
            break;
            case `Кардіолог`: doctor = 3;
            break;
        }
        localStorage.setItem(`visit_${doctor}_${visit.id}`, JSON.stringify(visit));
    }

    static removeFromLocalStorage(visit) {
        for (let i = 0; i < localStorage.length; i++) {
            let idFromKey = localStorage.key(i).split(`_`)[2];
            if (+idFromKey === +visit.id) {
                localStorage.removeItem(localStorage.key(i));
                return;
            }
        }
    }

    static createFromLocalStorage() {
        for (let i = 0; i < localStorage.length; i++) {
            Visit.count = 0;
            if (localStorage.key(i).split(`_`)[0] === `visit`) {
                let jsonObject = localStorage.getItem(localStorage.key(i));
                const regexp = new RegExp(`_`, `g`);
                jsonObject = jsonObject.replace(regexp, ``);
                const localObject = JSON.parse(jsonObject);
                let visit;
                console.log(localObject);
                const doctor = localStorage.key(i).split(`_`)[1];
                switch (+doctor) {
                    case 1:
                        visit = new VisitTherapist(localObject);
                        break;
                    case 2:
                        visit = new VisitDentist(localObject);
                        break;
                    case 3:
                        visit = new VisitCardiologist(localObject);
                        break;
                }
                Base.addVisit(visit);
                VisitDOM.createCard(visit);
            }
        }
    }
}

