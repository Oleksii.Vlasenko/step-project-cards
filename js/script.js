function select() {
    const selected = document.getElementById(`doctor`).selectedIndex;

    switch (selected) {
        case 1:
            VisitDOM.createForm(VisitTherapist.inputLines, 1);
            break;
        case 2:
            VisitDOM.createForm(VisitDentist.inputLines, 2);
            break;
        case 3:
            VisitDOM.createForm(VisitCardiologist.inputLines, 3);
            break;
        default:
            VisitDOM.clearForm();
    }
}

if (User.getUser() && User.getPassword()) {

    if (User.getToken()) {

        document.getElementsByClassName(`authorization`)[0].classList.remove(`active`);
        document.getElementsByClassName(`container`)[0].classList.add(`active`);
        let promise = Request.authorization(User.getUser(), User.getPassword());
        creatCardsFormServer();
    }
}

async function getToken() {

    const user = document.getElementById(`login`).value;
    const password = document.getElementById(`password`).value;
    let token;
    await Request.authorization(user, password)
        .then(response => response.json())
        .then(obj => {
            if (obj.status === `Success`) {
                token = obj.token;

                User.setUser(user);
                User.setPassword(password);
                User.setToken(token);

                document.getElementsByClassName(`authorization`)[0].classList.remove(`active`);
                document.getElementsByClassName(`container`)[0].classList.add(`active`);

                creatCardsFormServer();
            }
        })
        .catch(err => console.log(err));
}

function creatCardsFormServer() {

    let re = Request.sendGetRequest();
    re.then(response => response.json())
        .then(arr => {
            let json = JSON.stringify(arr);
            const regexp = new RegExp(`_`, `g`);
            json = json.replace(regexp, ``);
            arr = JSON.parse(json);
            if (!arr.length) {
                Local.createFromLocalStorage();
            } else {
                localStorage.clear();
                for (let i = 0; i < arr.length; i++) {
                    let visit;
                    // console.log(arr[i]);
                    switch (arr[i].doctor) {
                        case `Терапевт`:
                            visit = new VisitTherapist(arr[i]);
                            break;
                        case `Дантист`:
                            visit = new VisitDentist(arr[i]);
                            break;
                        case `Кардіолог`:
                            visit = new VisitCardiologist(arr[i]);
                            break;
                    }
                    // console.log(visit);
                    Base.addVisit(visit);
                    VisitDOM.createCard(visit);
                }
            }
        })
        .catch(err => {
            console.log(err);
            Local.createFromLocalStorage();
        });
}

    // let email = oleksii.vlasenkoua@gmail.com;
// let password = 12344344аа;
// let token = ed1f8bb1cfdf;

