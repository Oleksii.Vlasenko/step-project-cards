class Visit {

    constructor(title, visitDate, patient, comment, id) {
        this._title = title;
        this._visitDate = visitDate;
        this._patient = patient;
        this._comment = comment;
        this._id = id;
    }

    remove() {
        const index = Base.visits.indexOf(this);
        Base.splice(index, 1);
    }

    edit({title, visitDate, patient, comment}) {
        this._patient = patient;
        this._title = title;
        this._visitDate = visitDate;
        this._comment = comment;
    }
}