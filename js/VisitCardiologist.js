class VisitCardiologist extends Visit {

    static MEDICAL_SPECIALIZATION = `Кардіолог`;

    static inputLines = {
        title: [`Мета візиту`, `text`, `Вкажіть мету візиту пацієнта`],
        visitDate: [`Дата візиту`, `date`],
        patient: [`ПІП`, `text`, `Повне ім'я пацієнта`],
        bp: [`Тиск`, `number`, `Тиск пацієнта`],
        pressure: [`Захворювання серцево-судинної системи`, `text`, `Захворювання серцево-судинної системи`],
        massIndex: [`Індекс маси тіла`, `text`, `Індекс маси тіла пацієнта`],
        age: [`Вік`, `number`, `Вік пацієнта`]
    };

    constructor({title, visitDate, patient, bp, pressure, massIndex, age, comment, id}) {
        super(title, visitDate, patient, comment, id);
        this._bp = bp;
        this._pressure = pressure;
        this._massIndex = massIndex;
        this._age = age;
        this._doctor = VisitCardiologist.MEDICAL_SPECIALIZATION;
    }

    getDetails() {
        return {
            patient: this._patient,
            title: this._title,
            visitDate: this._visitDate,
            bp: this._bp,
            pressure: this._pressure,
            massIndex: this._massIndex,
            age: this._age
        }
    }

    edit({patient, title, visitDate, bp, pressure, massIndex, age, comment}) {
        super.edit({title, visitDate, patient, comment});
        this._bp = bp;
        this._pressure = pressure;
        this._massIndex = massIndex;
        this._age = age;
    }

    get comment() {
        return this._comment;
    }

    get patient() {
        return this._patient;
    }

    get doctor() {
        return this._doctor;
    }

    get id() {
        return this._id;
    }

    set id(id) {
        this._id = id;
    }

    get inputLines(){
        return VisitCardiologist.inputLines;
    }
}