class Base {
    static visits = [];

    static addVisit(visit) {
        Base.visits.push(visit);
    }

    static getVisitById(id) {
        for (let item of Base.visits) {
            if (+item.id === +id) {
                return item;
            }
        }
    }
}